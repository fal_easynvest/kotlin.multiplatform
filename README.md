# Kotlin Multiplatform
---
O Kotlin dispõe de um mecanismo que possibilita programar para diversas plataformas, como JVM, Android, iOS, JavaScript, dentre outras.

Este projeto tem a finalidade de explorar esta tecnologia, levantar suas vantagens e desvantagens, e verificar sua viabilidade para que possamos utilizá-lo em nossas aplicações.

## Estrutura
A estrutura desse projeto é composta por três aplicações: Back-end (JVM), apps Android e iOS.

## Todo
* Incluir mais detalhes na documentação
* Implementar o módulo para iOS