@file:Suppress("MayBeConstant")

import org.gradle.api.JavaVersion

object Versions {

    object Android {
        val min_sdk = 21
        val target_sdk = 30
        val compile_sdk = 30
        val gradle_plugin = "4.1.2"
    }

    object Java {
        val source = JavaVersion.VERSION_1_8
        val target = JavaVersion.VERSION_1_8
        val jvm_target = "1.8"
    }

    val kotlin = "1.4.31"
    val kotlinx_serialization = "1.1.0"
    val coroutines = "1.4.3"
    val ktor = "1.5.2"
    val material = "1.2.1"
    val appcompat = "1.2.0"
    val core_ktx = "1.3.2"
    val activity_ktx = "1.1.0"
    val compose = "1.0.0-alpha08"
    val compose_navigation = "1.0.0-alpha03"
    val ui_tooling = "1.0.0-alpha07"
    val uuid = "0.2.2"
    val moko_resources = "0.13.1"
    val moko_parcelize = "0.4.0"
    val moko_graphics = "0.4.0"
    val junit = "4.13"
    val shadow = "6.1.0"
}

object Deps {
    val kotlinx_serialization = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.kotlinx_serialization}"
    val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
    val material = "com.google.android.material:material:${Versions.material}"
    val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    val uuid = "com.benasher44:uuid:${Versions.uuid}"
    val reflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"
    val junit = "junit:junit:${Versions.junit}"

    object GradlePlugin {
        val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
        val android = "com.android.tools.build:gradle:${Versions.Android.gradle_plugin}"
        val moko = "dev.icerock.moko:resources-generator:${Versions.moko_resources}"
    }

    object Ktor {
        val server_netty = "io.ktor:ktor-server-netty:${Versions.ktor}"
        val serialization = "io.ktor:ktor-serialization:${Versions.ktor}"
        val client_android = "io.ktor:ktor-client-android:${Versions.ktor}"
        val client_cio = "io.ktor:ktor-client-cio:${Versions.ktor}"
        val client_serialization = "io.ktor:ktor-client-serialization:${Versions.ktor}"
    }

    object Ktx {
        val core = "androidx.core:core-ktx:${Versions.core_ktx}"
        val activity = "androidx.activity:activity-ktx:${Versions.activity_ktx}"
    }

    object Compose {
        val ui = "androidx.compose.ui:ui:${Versions.compose}"
        val foundation = "androidx.compose.foundation:foundation:${Versions.compose}"
        val material = "androidx.compose.material:material:${Versions.compose}"
        val material_icons_core = "androidx.compose.material:material-icons-core:${Versions.compose}"
        val material_icons_extended = "androidx.compose.material:material-icons-extended:${Versions.compose}"
        val runtime_livedata = "androidx.compose.runtime:runtime-livedata:${Versions.compose}"
        val ui_tooling = "androidx.ui:ui-tooling:${Versions.ui_tooling}"
        val navigation = "androidx.navigation:navigation-compose:${Versions.compose_navigation}"
    }

    object KotlinTest {
        val common = "org.jetbrains.kotlin:kotlin-test-common:${Versions.kotlin}"
        val annotations = "org.jetbrains.kotlin:kotlin-test-annotations-common:${Versions.kotlin}"
        val junit = "org.jetbrains.kotlin:kotlin-test-junit:${Versions.kotlin}"
    }

    object Moko {
        val resources = "dev.icerock.moko:resources:${Versions.moko_resources}"
        val parcelize = "dev.icerock.moko:parcelize:${Versions.moko_parcelize}"
        val graphics = "dev.icerock.moko:graphics:${Versions.moko_graphics}"
    }
}