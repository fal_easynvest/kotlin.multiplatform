plugins {
    id("org.gradle.application")
    id("com.github.johnrengelman.shadow") version Versions.shadow
    kotlin("multiplatform")
    kotlin("plugin.serialization") version Versions.kotlin
}

group = ProjectConfig.group
version = ProjectConfig.version

val backEndMainClass = "presenter.server.ServerKt"

setProperty("mainClassName", backEndMainClass)

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = Versions.Java.jvm_target
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
        withJava()
    }
    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(project(":common"))
                implementation(Deps.Ktor.server_netty)
                implementation(Deps.Ktor.serialization)
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation(Deps.KotlinTest.junit)
            }
        }
    }
}

application {
    mainClass.set(backEndMainClass)
}

java {
    sourceCompatibility = Versions.Java.source
    targetCompatibility = Versions.Java.target
}