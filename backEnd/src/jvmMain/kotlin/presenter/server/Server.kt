package presenter.server

import external.ktor.response.respondError
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.routing.routing
import io.ktor.serialization.json
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.serialization.SerializationException
import my.mpp.common.domain.error.InvalidJsonBodyError
import my.mpp.common.domain.error.core.ResponseError
import presenter.server.route.users

fun main() {
    embeddedServer(Netty, port = 8080) {
        install(ContentNegotiation) {
            json()
        }

        install(StatusPages) {
            exception<SerializationException> {
                call.respondError(InvalidJsonBodyError())
            }
            exception<ResponseError> { cause ->
                call.respondError(cause)
            }
        }

        routing {
            users()
        }
    }.start(wait = true)
}