package presenter.server.route

import domain.usecase.deleteUserUseCase
import domain.usecase.getUsersUseCase
import domain.usecase.insertUserUseCase
import domain.usecase.updateUserUseCase
import external.ktor.http.requireInt
import external.ktor.routing.post
import external.ktor.routing.put
import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.route
import my.mpp.common.domain.entity.User

fun Routing.users(): Route = route("/users") {
    query()
    insert()
    update()
    delete()
}

private fun Route.query(): Route = get {
    call.respond(getUsersUseCase())
}

private fun Route.insert(): Route = post { user: User ->
    call.respond(insertUserUseCase(user))
}

private fun Route.update(): Route = put("/{id}") { user: User ->
    call.respond(updateUserUseCase(call.parameters.requireInt("id"), user))
}

private fun Route.delete(): Route = delete("/{id}") {
    call.respond(deleteUserUseCase(call.parameters.requireInt("id")))
}