package external.ktor.response

import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import my.mpp.common.domain.error.core.ResponseError

public suspend inline fun <reified T : ResponseError> ApplicationCall.respondError(error: T) {
    response.status(HttpStatusCode.fromValue(error.statusCode))
    respond(error)
}