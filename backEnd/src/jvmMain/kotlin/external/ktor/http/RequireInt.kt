package external.ktor.http

import io.ktor.http.Parameters

fun Parameters.requireInt(name: String): Int =
    get(name)?.toIntOrNull() ?: error("Param $name not found or is a invalid Int.")