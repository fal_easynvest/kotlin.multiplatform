package domain.usecase

import my.mpp.common.domain.entity.User
import my.mpp.common.domain.error.UserNotFoundError

fun getUsersUseCase(): List<User> = userList.toList()

fun insertUserUseCase(user: User): User {
    val maxId = userList
        .maxByOrNull { it.id }
        ?.id ?: 0

    user.id = maxId + 1

    userList.add(user)

    return user
}

fun updateUserUseCase(id: Int, user: User): User {
    userList
        .indexOfFirst { id == it.id }
        .takeIf { it != -1 }
        ?.also {
            user.id = id
            userList[it] = user
        }
        ?: throw UserNotFoundError()

    return user
}

fun deleteUserUseCase(id: Int) {
    if (!userList.removeIf { it.id == id })
        throw UserNotFoundError()
}

val userList = mutableListOf(
    User(
        id = 1,
        name = "Felipe",
        username = "f0x",
        email = "felipe.fox@email.com"
    )
)