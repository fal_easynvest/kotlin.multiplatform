plugins {
    id("com.android.application")
    kotlin("android")
}

group = ProjectConfig.group
version = ProjectConfig.version

dependencies {
    implementation(project(":common"))
    implementation(Deps.material)
    implementation(Deps.Ktor.client_android)
    implementation(Deps.Ktor.client_cio)
    implementation(Deps.Ktor.client_serialization)
    testImplementation(Deps.junit)
}

android {
    compileSdkVersion(Versions.Android.compile_sdk)
    defaultConfig {
        applicationId = "my.mpp.android.app"
        minSdkVersion(Versions.Android.min_sdk)
        targetSdkVersion(Versions.Android.target_sdk)
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = Versions.Java.source
        targetCompatibility = Versions.Java.target
    }
}