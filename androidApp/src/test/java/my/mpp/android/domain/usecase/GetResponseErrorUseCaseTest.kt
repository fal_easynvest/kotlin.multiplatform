package my.mpp.android.domain.usecase

import my.mpp.common.domain.error.InvalidJsonBodyError
import my.mpp.common.domain.error.UserNotFoundError
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class GetResponseErrorUseCaseTest {

    @Test
    fun `on get response error, with valid error format with code 1100, should return UserNotFoundError`() {
        val errorResponse = getResponseErrorUseCase(
            """
            {
                "error": "User not found.",
                "code": 11000
            }
            """.trimIndent()
        )

        assertThat(errorResponse, instanceOf(UserNotFoundError::class.java))
    }

    @Test
    fun `on get response error, with valid error format with code 1000, should return InvalidJsonBodyError`() {
        val errorResponse = getResponseErrorUseCase(
            """
            {
                "error": "Invalid request body. There are missing or invalid fields, or JSON is not valid.",
                "code": 1000
            }
            """.trimIndent()
        )

        assertThat(errorResponse, instanceOf(InvalidJsonBodyError::class.java))
    }

}