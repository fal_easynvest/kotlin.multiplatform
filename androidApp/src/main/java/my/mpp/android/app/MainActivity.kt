package my.mpp.android.app

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.defaultRequest
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.user.common.getPlatformName
import my.mpp.common.domain.entity.User

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("mpp-androidApp", "Hello from MPP ${getPlatformName()}")

        CoroutineScope(Dispatchers.Main).launch {
            HttpClient(CIO) {
                install(JsonFeature) {
                    serializer = KotlinxSerializer()
                }

                defaultRequest {
                    url {
                        host = "192.168.1.190"
                        port = 8080
                    }
                }
            }.apply {
                try {
                    val users = use { get<List<User>>(path = "/users") }
                    Log.d("mpp-androidApp", "Users from server: $users")
                } catch (e: Throwable) {
                    Log.d("mpp-androidApp", "Users from server: failed")
                }
            }
        }
    }

}