pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

rootProject.name = "my-mpp"

include(":common")
include(":backEnd")
include(":androidApp")