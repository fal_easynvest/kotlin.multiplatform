buildscript {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        google()
    }
    dependencies {
        classpath(Deps.GradlePlugin.kotlin)
        classpath(Deps.GradlePlugin.android)
    }
}

group = ProjectConfig.group
version = ProjectConfig.version

@Suppress("JcenterRepositoryObsolete")
allprojects {
    repositories {
        jcenter()
        mavenCentral()
        google()
    }
}