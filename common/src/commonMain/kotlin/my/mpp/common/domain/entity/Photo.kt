package my.mpp.common.domain.entity

import kotlinx.serialization.Serializable

@Serializable
data class Photo(
    var albumId: Int,
    var id: Int,
    var title: String,
    var url: String,
    var thumbnailUrl: String
)