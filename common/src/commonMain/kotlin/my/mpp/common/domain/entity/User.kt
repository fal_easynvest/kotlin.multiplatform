package my.mpp.common.domain.entity

import kotlinx.serialization.Serializable

@Serializable
data class User(
    var id: Int = 0,
    var name: String,
    var username: String,
    var email: String
)