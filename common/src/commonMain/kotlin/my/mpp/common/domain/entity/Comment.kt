package my.mpp.common.domain.entity

import kotlinx.serialization.Serializable

@Serializable
data class Comment(
    var postId: Int,
    var id: Int,
    var name: String,
    var email: String,
    var body: String
)