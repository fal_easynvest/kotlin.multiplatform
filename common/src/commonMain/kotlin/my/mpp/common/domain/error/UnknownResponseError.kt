package my.mpp.common.domain.error

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import my.mpp.common.domain.error.core.ResponseError
import my.mpp.common.domain.error.core.ResponseErrorReference

@Serializable
class UnknownResponseError(
    @Transient
    override val statusCode: Int = 0,
    override val error: String = "Unknown error.",
    override val code: Int = errorCode
) : ResponseError() {

    companion object : ResponseErrorReference<UnknownResponseError> {

        override val errorCode = 0

    }

}