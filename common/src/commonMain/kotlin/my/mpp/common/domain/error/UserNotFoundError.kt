package my.mpp.common.domain.error

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import my.mpp.common.domain.error.core.ResponseError
import my.mpp.common.domain.error.core.ResponseErrorReference

@Serializable
class UserNotFoundError(
    @Transient
    override val statusCode: Int = 400,
    override val error: String = "User not found."
) : ResponseError() {

    override val code = errorCode

    companion object: ResponseErrorReference<UserNotFoundError> {

        override val errorCode = 11000

    }

}