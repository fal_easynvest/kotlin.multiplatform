package my.mpp.common.domain.error.core

abstract class ResponseError : Throwable() {

    abstract val statusCode: Int

    abstract val code: Int

    abstract val error: String

    override val message: String?
        get() = error

}