package my.mpp.common.domain.entity

import kotlinx.serialization.Serializable

@Serializable
data class Album(
    var userId: Int,
    var id: Int,
    var title: String
)