package my.mpp.common.domain.error.core

interface ResponseErrorReference<T : ResponseError> {

    val errorCode: Int

}