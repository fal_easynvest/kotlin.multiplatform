package my.mpp.common.domain.error

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import my.mpp.common.domain.error.core.ResponseError
import my.mpp.common.domain.error.core.ResponseErrorReference

@Serializable
class InvalidJsonBodyError(
    @Transient
    override val statusCode: Int = 400,
    override val error: String = "Invalid request body. There are missing or invalid fields, or JSON is not valid."
) : ResponseError() {

    override val code = errorCode

    companion object : ResponseErrorReference<InvalidJsonBodyError> {

        override val errorCode = 1000

    }

}