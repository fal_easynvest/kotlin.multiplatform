package my.mpp.android.domain.usecase

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json

fun <T> decodeJsonFromStringWithSerializerUseCase(
    content: String,
    serializer: KSerializer<T>
): T =
    Json.decodeFromString(serializer, content)