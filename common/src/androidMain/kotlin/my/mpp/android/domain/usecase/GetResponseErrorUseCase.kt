package my.mpp.android.domain.usecase

import kotlinx.serialization.KSerializer
import kotlinx.serialization.serializer
import my.mpp.common.domain.error.InvalidJsonBodyError
import my.mpp.common.domain.error.UnknownResponseError
import my.mpp.common.domain.error.UserNotFoundError
import my.mpp.common.domain.error.core.ResponseError
import my.mpp.common.domain.error.core.ResponseErrorReference

internal fun mapErrorResponse(
    block: ErrorResponseMapper.() -> Unit
): ErrorResponseMapper {
    return ErrorResponseMapper().apply(block)
}

val errorResponseMapper = mapErrorResponse {
    map { InvalidJsonBodyError }
    map { UserNotFoundError }
}

fun getResponseErrorUseCase(responseBody: String): ResponseError =
    getErrorResponse(responseBody, errorResponseMapper)

internal fun getErrorResponse(
    responseBody: String,
    errorResponseMapper: ErrorResponseMapper
): ResponseError = errorResponseMapper.run {
    val unknownError = decodeJsonFromStringUseCase<UnknownResponseError>(responseBody)

    serializationData.forEach {
        if (unknownError.code == it.reference.errorCode)
            return decodeJsonFromStringWithSerializerUseCase(
                responseBody,
                it.serializer
            )
    }

    unknownError
}

class ErrorResponseMapper {

    val serializationData = mutableListOf<SerializationDataHolder<*>>()

    inline fun <reified T : ResponseError> map(
        noinline identifiableError: () -> ResponseErrorReference<T>
    ) = serializationData.add(SerializationDataHolder(identifiableError(), serializer()))

}

class SerializationDataHolder<T : ResponseError>(
    val reference: ResponseErrorReference<T>,
    val serializer: KSerializer<T>,
)