package my.mpp.android.domain.usecase

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

inline fun <reified T> decodeJsonFromStringUseCase(content: String): T =
    Json.decodeFromString(content)